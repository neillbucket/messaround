import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {

  apiURL:string = 'https://swapi.co/api/'

  constructor(private http:HttpClient) { }

  //declare methods of this service 
  private handleError<T> (operation = 'operation', result?: T){
    return (error:any): Observable<T> => {
      console.error(error);
      alert("This item could not be found")
      return throwError(
        "Your request could not be found. Sorry please try again"
      )
    };
  }
  getData(){
    return this.http.get(`${this.apiURL}species`).pipe(
      catchError(this.handleError<Object[]>('getData', []))
    );
  }

  getParamData(id, category){
    return this.http.get(`${this.apiURL}${category}/${id}`).pipe(
      catchError(this.handleError<Object[]>('getParamData', []))
    );
  }
}
